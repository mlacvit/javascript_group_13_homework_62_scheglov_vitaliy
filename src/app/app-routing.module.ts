import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameComponent } from './game/game.component';
import { GameItemComponent } from './game/game-item/game-item.component';
import { NewGameComponent } from './new-game/new-game.component';
import { PlatformComponent } from './platform/platform.component';
import { HomeComponent } from './home/home.component';
import { AllGameComponent } from './all-game/all-game.component';


const routes: Routes = [
  {path: '', component: HomeComponent, children: [
      {path: '', component: GameComponent},
      {path: 'all', component: AllGameComponent},
      {path: 'new-game', component: NewGameComponent},
      {path: ':platform', component: PlatformComponent, children: [
          {path: ':id', component: GameItemComponent},
        ]},
    ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
