import { Component, OnInit } from '@angular/core';
import { GameModel } from '../game.model';
import { GameServices } from '../game.services';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-platform',
  templateUrl: './platform.component.html',
  styleUrls: ['./platform.component.css']
})
export class PlatformComponent implements OnInit {
  plat = '';
  game: GameModel[] = [];
  constructor(private route: ActivatedRoute, private service: GameServices) { }

  ngOnInit(): void {
    this.route.params.subscribe((params:Params) =>{
      this.plat = params['platform'];
      this.getGames();
    })
    this.getGames()
  }
  getGames(){
    this.service.platformGame = [];
    this.game = this.service.getGamesByPlatform(this.plat)
  }

}

