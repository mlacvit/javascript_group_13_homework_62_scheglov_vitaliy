import { Component, OnInit } from '@angular/core';
import { GameModel } from '../game.model';
import { GameServices } from '../game.services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  game: GameModel[] = [];
  gamePlat: string[] = [];

  constructor(private services: GameServices) { }

  ngOnInit(): void {
    this.game = this.services.getGames();
    this.services.gameChange.subscribe((game: GameModel[]) => {
      this.game = game;
    });
    this.platform()
  }

  platform(){
    this.game.forEach((game: GameModel)=>{
      this.gamePlat.push(game.platform)
    })
  }

  clickPlat(plat: string) {
    this.services.platform = plat;
    this.services.platformGame = [];
  }
}
