export class GameModel {
  constructor(
   public nameGame: string,
   public imageUrl: string,
   public description: string,
   public platform: string) {
  }
}
