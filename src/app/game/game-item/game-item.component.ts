import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { GameServices } from '../../game.services';
import { GameModel } from '../../game.model';

@Component({
  selector: 'app-game-item',
  templateUrl: './game-item.component.html',
  styleUrls: ['./game-item.component.css']
})
export class GameItemComponent implements OnInit {
  game!: GameModel;
  constructor(private route: ActivatedRoute, private service: GameServices) { }

  ngOnInit(): void {
    this.route.params.subscribe((params:Params) =>{
      const gameId = parseInt(params['id']);
      this.gameInfo(gameId);
    })

  }
   gameInfo(index: number){
    this.game = this.service.platformGame[index];
   }
}
