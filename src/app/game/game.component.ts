import { Component, OnInit } from '@angular/core';
import { GameServices } from '../game.services';
import { GameModel } from '../game.model';


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit  {
  game: GameModel[] = [];
  constructor(private services: GameServices) { }

  ngOnInit(): void {
    this.game = this.services.getGames();
    this.services.gameChange.subscribe((game: GameModel[]) => {
      this.game = game;
    });
  }
}
