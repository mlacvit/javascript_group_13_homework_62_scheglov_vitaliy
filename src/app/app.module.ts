import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewGameComponent } from './new-game/new-game.component';
import { GameComponent } from './game/game.component';
import { GameItemComponent } from './game/game-item/game-item.component';
import { GameServices } from './game.services';
import { MenuComponent } from './menu/menu.component';
import { PlatformComponent } from './platform/platform.component';
import { HomeComponent } from './home/home.component';
import { AllGameComponent } from './all-game/all-game.component';



@NgModule({
  declarations: [
    AppComponent,
    NewGameComponent,
    GameComponent,
    GameItemComponent,
    MenuComponent,
    PlatformComponent,
    HomeComponent,
    AllGameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

  ],
  providers: [GameServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
