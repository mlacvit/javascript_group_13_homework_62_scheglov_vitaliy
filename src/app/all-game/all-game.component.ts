import { Component, OnInit } from '@angular/core';
import { GameModel } from '../game.model';
import { GameServices } from '../game.services';

@Component({
  selector: 'app-all-game',
  templateUrl: './all-game.component.html',
  styleUrls: ['./all-game.component.css']
})
export class AllGameComponent implements OnInit {
  game: GameModel[] = [];
  constructor(private services: GameServices) { }

  ngOnInit(): void {
    this.game = this.services.getGames();
    this.services.gameChange.subscribe((game: GameModel[]) => {
      this.game = game;
    });
  }

}
