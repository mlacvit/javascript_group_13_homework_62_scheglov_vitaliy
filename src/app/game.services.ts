import { GameModel } from './game.model';
import { EventEmitter } from '@angular/core';

export class GameServices {
  gameChange = new EventEmitter<GameModel[]>();
  platform = '';
  platformGame: GameModel[] = [];
  private newGame: GameModel[]  = [
   new GameModel('Mario', 'https://images-na.ssl-images-amazon.com/images/I/81OYz9kILxL.jpg','Mario) — персонаж видеоигр компании Nintendo, созданный Сигэру Миямото. Являясь талисманом Nintendo и основным героем серии, Марио появился в более чем 200 видеоиграх с момента своего создания. Впервые появился в аркаде Donkey Kong под изначальным именем Прыгун (англ. ... С 1995 года Марио озвучивается Чарльзом Мартине.', 'Super Nintendo'),
   new GameModel( 'Ферма', 'https://img1.labirint.ru/books/295961/scrn_big_1.jpg',  'участники выступают в роли фермеров, каждый из которых развивает небольшое приусадебное хозяйство. Начиная с одной курочки, вы постепенно увеличиваете поголовье рогатого, копытного, мычащего, блеющего и хрюкающего скота.', 'NES' ),
    new GameModel('Mafia 2', 'https://upload.wikimedia.org/wikipedia/ru/d/d6/MafiaII.jpg', 'Mafia 2 - это приключенческий экшен, который переносит игроков в преступный мир Америки 40-50-х годов прошлого века. События игры стартуют в феврале 1945 года, когда еще идет Вторая Мировая война. Вито Скарлетта, главный герой игры, еще молод и неопытен, он только что вернулся из армии и жаждет найти себя в жизни.', 'Sony PlayStation'),
    new GameModel('Counter-Strike', 'https://brotorrent.net/uploads/posts/2019-02/1549355126_counter-strike-1_6-original-1.png', 'онлайн-шутер, суть которого заключается в противостоянии двух команд – террористов и контртеррористов (спецназа). У обеих команд есть цели в раунде, завершив которые, либо полностью уничтожив команду противника, они получают матч-поинт. Выиграв определенное количество раундов, команда выигрывает матч.', 'PC'),
    new GameModel('Mass Effect', 'https://images.thedirect.com/media/article_full/masseffect.jpg', 'оэто мастшабная ролевая игра от BioWare на тему космических приключений. История повествует о далеком будущем, где человечество занимает далеко не лидирующее положении среди межрасового альянса, которым управляет так назыываемый "Совет Цитадели', 'SEGA Genesis'),
    ];

  getGames() {
    return this.newGame.slice();
  }

  getGameIndex(index:number){
    return this.newGame[index]
  }

  addGame(game: GameModel){
     this.newGame.push(game);
     this.gameChange.emit(this.newGame)
  }

  getGamesByPlatform(platform: string){
    this.newGame.forEach((game:GameModel) =>{
     if (game.platform === this.platform){
       this.platformGame.push(game)
     }
    })
    return this.platformGame;
  }
}
