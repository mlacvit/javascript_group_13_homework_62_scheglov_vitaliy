import { Component, ElementRef, ViewChild } from '@angular/core';
import { GameServices } from '../game.services';
import { GameModel } from '../game.model';



@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.css']
})
export class NewGameComponent {
  @ViewChild('nameGame') nameGame!: ElementRef;
  @ViewChild('imageUrl') imageUrl!: ElementRef;
  @ViewChild('platform') platform!: ElementRef;
  @ViewChild('description') description!: ElementRef;

  constructor(private service: GameServices) { }

  pushNewGame(e: Event) {
    e.preventDefault();
    const name: string = this.nameGame.nativeElement.value;
    const imageUrl: string = this.imageUrl.nativeElement.value;
    const description: string = this.description.nativeElement.value;
    const platform: string = this.platform.nativeElement.value;
    const newGame = new GameModel(name, imageUrl, description, platform)
    this.service.addGame(newGame);
  }

}
